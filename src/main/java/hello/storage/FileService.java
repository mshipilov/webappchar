package hello.storage;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import hello.network.Data;
import hello.network.ResponseCharJson;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;


public class FileService implements DataService {

    private String pathToFolder = System.getProperty("user.dir") +  "\\files\\";
    private final String titlesArrayNameInJson = "titles";
    private final String arrayNameInJson = "array";
    private final String separator = ",";

    @Override
    public File saveAsFile(MultipartFile file) {
        File convFile = new File(pathToFolder+file.getOriginalFilename());
        try {
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
            return convFile;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<String> getHeadersFromFile(File file) {
        String extension = FilenameUtils.getExtension(file.getPath());
        switch (extension){
            case "csv": return getDataFromCSVFile(file);
            case "json": return getDataFromJsonFile(file);
            case "xlsx": return getDataFromXLSLFile(file);
        }
        return null;
    }

    private List<String> getDataFromXLSLFile(File file){
        List<String> list = new ArrayList();
        try {

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            String[] header = datatypeSheet.getRow(0).getCell(0).toString().split(separator);
            for (String str : header){
                list.add(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(list.toString());
        return list;
    }

    private List<String> getDataFromCSVFile(File file) {
        List<String> list = new ArrayList();
        Reader reader = createFileReader(file);
        CSVParser csvFileParser = null;
        try {
            csvFileParser = CSVFormat.EXCEL.parse(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Iterator iterator = csvFileParser.getRecords().get(0).iterator();
            while (iterator.hasNext()){
                list.add(iterator.next().toString());
            }
            return list;
        } catch (IOException e) {
            return  null;
        }
    }

    private Reader createFileReader(File file) {
        Reader in = null;
        try {
            in = new FileReader(file.getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return in;
    }

    @Override
    public List<Object> getListObjectFromCsv(String filename) {
        File input = new File(pathToFolder+filename);
        CsvSchema csvSchema = CsvSchema.builder().setUseHeader(true).build();
        CsvMapper csvMapper = new CsvMapper();

        List<Object> readAll = null;
        try {
            readAll = csvMapper.readerFor(Map.class).with(csvSchema).readValues(input).readAll();
            return readAll;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<String> getDataFromJsonFile(File file){
        List<String> list = new ArrayList();
        Reader reader = createFileReader(file);
        JSONObject jsonObject = parseJsonFile(reader);
        Iterator iterator = ((JSONArray) jsonObject.get(titlesArrayNameInJson)).iterator();
        while(iterator.hasNext()){
            list.add((String) iterator.next());
        }
        return list;
    }

    private JSONObject parseJsonFile(Reader reader){
        JSONParser parser = new JSONParser();
        try {
            return  (JSONObject) parser.parse(reader);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }

    private JSONObject parseJsonFile(String filename){
        try {
            return parseJsonFile(new FileReader(pathToFolder+filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Object> getListObjectFromJson(String filename) {
        List<Object> list = new ArrayList();
        JSONObject jsonObj = parseJsonFile(filename);
        Iterator dataIterator = ((JSONArray)jsonObj.get(arrayNameInJson)).iterator();
        JSONArray titlesIterator = ((JSONArray)jsonObj.get(titlesArrayNameInJson));

        while(dataIterator.hasNext()){
            LinkedHashMap hashMap = new LinkedHashMap();
            JSONObject object = (JSONObject) dataIterator.next();

            titlesIterator.forEach(item -> {
                hashMap.put(item,object.get(item));
            });

            list.add(hashMap);
        }
        return list;
    }

    //TODO дописать метод для получения данных из excel
    @Override
    public List<Object> getListObjectFromXlsx(String filename) {
        try {

            FileInputStream excelFile = new FileInputStream(new File(pathToFolder + filename));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();

            while (iterator.hasNext()) {

                Row currentRow = iterator.next();
                String[] row = currentRow.getCell(0).toString().split(separator);
                for (String str : row){
                    LinkedHashMap hashMap = new LinkedHashMap();
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseCharJson getResponseChar(List<Data> listParams, String filename) {
        List<Object> listOfObj = null;
        String extension = FilenameUtils.getExtension(filename);
        switch (extension){
            case "csv": listOfObj = getListObjectFromCsv(filename); break;
            case "json": listOfObj = getListObjectFromJson(filename); break;
            case "xlsx": listOfObj = getListObjectFromXlsx(filename); break;
        }
        return new ResponseCharJson(listOfObj,listParams);
    }
}
