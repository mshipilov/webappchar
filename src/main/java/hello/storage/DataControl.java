package hello.storage;

import hello.configuration.FileServiceConfiguration;
import hello.network.Data;
import hello.network.ResponseCharJson;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

@Component
public class DataControl implements DataSource {

    private DataService dataService = FileServiceConfiguration.createDataService();

    @Override
    public ResponseCharJson getResponseChar(List<Data> listParams, String filename) {
        return dataService.getResponseChar(listParams,filename);
    }

    @Override
    public File saveData(MultipartFile file) {
        return dataService.saveAsFile(file);
    }

    @Override
    public List<String> getHeaders(File file) {
        return dataService.getHeadersFromFile(file);
    }
}
