package hello.storage;

import hello.network.Data;
import hello.network.ResponseCharJson;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.util.List;

@Service
public interface DataService {
    File saveAsFile(MultipartFile file);
    List<String> getHeadersFromFile(File file);
    ResponseCharJson getResponseChar(List<Data> listParams, String filename);
    List<Object> getListObjectFromCsv(String filename);
    List<Object> getListObjectFromJson(String filename);
    List<Object> getListObjectFromXlsx(String filename);
}
