package hello.storage;

import hello.network.Data;
import hello.network.ResponseCharJson;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

@Service
public interface DataSource {
    ResponseCharJson getResponseChar(List<Data> listParams, String filename);
    File saveData(MultipartFile file);
    List<String> getHeaders(File file);
}
