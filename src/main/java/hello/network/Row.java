package hello.network;

import java.util.ArrayList;
import java.util.List;

public class Row {
    private List c = new ArrayList();

    public Row(String id, int value){
        c.add(new CellName(id));
        c.add( new CellValue(value));
    }

    public List getC() {
        return c;
    }

    public void setC(List c) {
        this.c = c;

    }

    class CellName{
        private String v;

        public CellName(String v) {
            this.v = v;
        }

        public String getV() {
            return v;
        }

        public void setV(String v) {
            this.v = v;
        }
    }

    class CellValue{
        private int v;

        public CellValue(int v) {
            this.v = v;
        }

        public int getV() {
            return v;
        }

        public void setV(int v) {
            this.v = v;
        }
    }
}
