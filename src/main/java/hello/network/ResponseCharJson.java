package hello.network;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ResponseCharJson {
    private List<Row> rows;
    private List<Col> cols;

    public ResponseCharJson(List<Object> listOfParams, List<Data> dataList){
        this.rows = new ArrayList<>();
        this.cols = new ArrayList<>();
        generateCols(dataList);
        generateRows(listOfParams);
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public List<Col> getCols() {
        return cols;
    }

    public void setCols(List<Col> cols) {
        this.cols = cols;
    }

    private void generateRows(List<Object> list){
        String typeNumber = null;
        String typeString = null;
        for (Col col : cols){
            if (col.getType().equals("number")){
                typeNumber = col.getId();
            }else if (col.getType().equals("string")){
                typeString = col.getId();
            }
        }

        for (Object object: list){
            LinkedHashMap hashMap = (LinkedHashMap)object;
            String value = String.valueOf(hashMap.get(typeNumber));
            int valueOf = 0;
            if (value!=null){
                valueOf = Integer.valueOf(value);
            }
            rows.add(new Row(String.valueOf(hashMap.get(typeString)),valueOf));
            System.out.println(hashMap);
        }
    }

    private void generateCols(List<Data> list){
        for (Data data: list){
            cols.add(new Col(data.getName(),data.getType(),data.getName()));
        }
    }

}
