package hello.configuration;

import hello.storage.DataControl;
import hello.storage.DataService;
import hello.storage.DataSource;
import hello.storage.FileService;

public class FileServiceConfiguration {


    public static DataService createDataService() {
        return new FileService();
    }

    public static DataSource createDataSource() {
        return new DataControl();
    }
}
