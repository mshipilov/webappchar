package hello;

import hello.storage.FileService;
import hello.storage.DataService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.*;

@Controller
public class GreetingController {

    private DataService dataService = new FileService();

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @PostMapping("/greeting")
    public String testMethod(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {
        File CSVFile = dataService.saveAsFile(file);
        dataService.getHeadersFromFile(CSVFile);
        return "redirect:/";
    }

    @GetMapping("/")
    public String home(){
        return "index";
    }
}
