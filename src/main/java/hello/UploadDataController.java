package hello;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import hello.configuration.FileServiceConfiguration;
import hello.network.Data;
import hello.network.ResponseCharJson;
import hello.storage.DataSource;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

@Controller
public class UploadDataController {

    private DataSource dataSource = FileServiceConfiguration.createDataSource();

    @RequestMapping(value = "/upload_data", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<String>> uploadFile(
            @RequestParam("uploadfile") MultipartFile uploadfile) {
        if (uploadfile == null){
            return ResponseEntity
                    .ok()
                    .cacheControl(CacheControl.noCache())
                    .body(null);
        }
        File file = dataSource.saveData(uploadfile);
        List<String> headers = dataSource.getHeaders(file);
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.noCache())
                .body(headers);
    }

    @RequestMapping(value = "/get_data", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ResponseCharJson> getData(@RequestBody String stuffs,
                                                    @RequestParam String uploadfile) {

        Type itemsListType = new TypeToken<List<Data>>() {}.getType();
        List<Data> listItemsDes = new Gson().fromJson(stuffs,itemsListType);
        ResponseCharJson responseCharJson = dataSource.getResponseChar(listItemsDes,uploadfile);
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.noCache())
                .body(responseCharJson);
    }
}
