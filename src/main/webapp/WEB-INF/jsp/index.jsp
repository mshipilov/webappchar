<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Chars</title>
     <%@ page contentType="text/html;charset=utf-8" %>
     <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
     <script type="text/javascript" src="js/char.js"></script>

</head>
<body>
    <script>
    $(document).ready(function() {
                         $("#upload-file-input").on("change", uploadFile);
                         $(".Blocks").hide();
                         $("#submit").click(function(){
                              array  = [];
                              $('.Blocks').each(function(){
                                     array.push({
                                            name: $(this).find(".Name").val(),
                                            type: $(this).find(".Type").val()
                                     });
                              });
                              var jsonString = JSON.stringify(array );
                              $.ajax
                                  ({
                                      type: "POST",
                                      url: '/get_data?uploadfile=' + $('input[type=file]').val().replace(/.*(\/|\\)/, ''),
                                      dataType: 'json',
                                      contentType: 'application/json',
                                      data: jsonString,
                                      success: function (data) {
                                      var columns = data.cols
                                      var rows = data.rows



                                            google.charts.load('current', {'packages':['corechart']});
                                            google.charts.setOnLoadCallback(drawChart);

                                            function drawChart() {
                                            var dataTable = new google.visualization.DataTable();

                                      columns.forEach(function(col) {
                                       console.log(col);
                                       dataTable.addColumn(col.type, col.label);
                                     });

                                     var arrayData = []
                                     rows.forEach(function(rows) {
                                         console.log(rows.c[0].v);
                                         console.log(rows.c[1].v);
                                         var arrayObj = [rows.c[0].v, rows.c[1].v];
                                         arrayData.push(arrayObj);
                                     });
                                     console.log(arrayData);
                                     dataTable.addRows(arrayData);


                                             var options = {
                                                title: 'Company Performance',
                                                hAxis: {titleTextStyle: {color: '#333'}},
                                                vAxis: {minValue: 0}
                                              };

                                              var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                                              chart.draw(dataTable, options);

                                            }

                                      },
                                      error: function (data) {
                                           alert("Что-то пошло не так :(");
                                       }
                                  })
                         });
                       });

                       function uploadFile() {
                         $.ajax({
                           url: "/upload_data",
                           type: "POST",
                           data: new FormData($("#upload-file-form")[0]),
                           enctype: 'multipart/form-data',
                           processData: false,
                           contentType: false,
                           cache: false,
                           success: function (data) {
                              $('.Name').each(function(){
                                  for (var i in data) {
                                       $(this).append('<option value=' + data[i] + '>' + data[i] + '</option>');
                                  }
                              });

                             $(".Blocks").show();
                           },
                           error: function () {
                            alert("Что-то пошло не так :(");
                           }
                         });
    }
    </script>
    <div>
        <form id="upload-file-form">
         Выберите файл с данными:</td><td> <input id="upload-file-input" type="file" name="uploadfile" accept="*" />
         <div class = "Blocks">
           <p>Выберите ось X: <select class="Name">
             </select></p>
           <p>Тип: <select class="Type">
               <option value="" selected disabled hidden>тип</option>
               <option>string</option>
               <option>number</option>
                            </select></p>
         </div>
         <div class = "Blocks">
           <p>Выберите ось Y: <select class="Name">
             </select></p>
           <p> Тип: <select class="Type">
                         <option value="" selected disabled hidden>тип</option>
                         <option>string</option>
                         <option>number</option>
                                       </select></p>
           <p><input type="button" value="Построить график" id = "submit"></p>
         <div>
        </form>
    </div>




 <div id="chart_div" style="width: 100%; height: 500px;"></div>
</body>
</html>
